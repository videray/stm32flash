LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := stm32flash
LOCAL_MODULE_PATH := $(TARGET_RECOVERY_ROOT_OUT)/sbin
LOCAL_C_INCLUDES := $(LOCAL_PATH)/parsers
LOCAL_SRC_FILES :=	\
	dev_table.c	\
	i2c.c		\
	init.c		\
	main.c		\
	port.c		\
	serial_common.c	\
	serial_platform.c	\
	stm32.c		\
	utils.c \
	parsers/binary.c \
	parsers/hex.c \

LOCAL_FORCE_STATIC_EXECUTABLE := true

include $(BUILD_EXECUTABLE)
